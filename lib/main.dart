import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:timetabler/screens/auth_screen.dart';
import 'package:timetabler/screens/main.dart';
import 'package:timetabler/screens/profile_screen.dart';
import 'package:timetabler/screens/other_profile.dart';
import 'package:timetabler/screens/teachers_list.dart';
import 'package:timetabler/screens/about_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:timetabler/screens/chat_rooms.dart';
import 'package:timetabler/screens/search.dart';
import 'screens/chat.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  static final String root = '/';
  static final String auth = '/auth';
  static final String main = '/main';
  static final String profile = '/profile_screen';
  static final String otherProf = '/other_profile';
  static final String teachers = '/teachers_list';
  static final String aboutIt = '/about_screen';
  static final String chatRoom = '/chat_rooms';
  static final String chatScreen = '/chat';
  static final String search = '/search';
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
          primaryColor: Colors.orange
      ),
      initialRoute: root,
      routes: {
        root : (_) => _SplashScreen(),
        auth : (_) => AuthScreen(),
        main : (context){
          return MainScreen();
        },
        profile : (context){
          return ProfileScreen();
        },
        teachers : (context){
          return TeachersList();
        },
        aboutIt : (context){
          return AboutProgram();
        },
        otherProf : (context){
          return OtherProfileScreen();
        },
        chatRoom : (context){
          return ChatRooms();
        },
        search : (context){
          return searchScreen();
        },
      },
    );
  }
}

class _SplashScreen extends StatefulWidget {
  @override
  __SplashScreenState createState() => __SplashScreenState();
}

class __SplashScreenState extends State<_SplashScreen> with TickerProviderStateMixin {
  @override
  void initState() {
    //Logo animation
    _controller = AnimationController(vsync: this, duration: Duration(seconds: 1));
    logoAnimation =  Tween<Offset>(
        begin: Offset(0, 0.5),
        end: Offset.zero
    ).animate(CurvedAnimation(parent: _controller, curve: Curves.elasticInOut));


    //Star animation
    _starAnimationController = AnimationController(vsync: this, duration: Duration(seconds: 5));
    starsAnimation = CurvedAnimation(parent: _starAnimationController, curve: Curves.linear);
    _starAnimationController.repeat();
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      _controller.forward();
      _controller.addStatusListener((status) async {
        if (status == AnimationStatus.completed) {
          final storage = FlutterSecureStorage();
          try{
            final login = await storage.read(key: 'login');
            final pwd = await storage.read(key: 'pwd');
            if(login != null && pwd != null){
              await FirebaseAuth.instance.signInWithEmailAndPassword(
                  email: login,
                  password: pwd
              );
              Navigator.of(context).pushNamedAndRemoveUntil(MyApp.main, (route) => false);
              print('Login: success');
            }else
              Navigator.pushNamedAndRemoveUntil(context, MyApp.auth, (_) => false);
          }catch(e){
            print('Error: $e');
            Navigator.pushNamedAndRemoveUntil(context, MyApp.auth, (_) => false);
          }
        }
      });
    });
  }
  AnimationController _controller;
  AnimationController _starAnimationController;
  Animation<Offset> logoAnimation;
  Animation<double> starsAnimation;

  @override
  void dispose() {
    _starAnimationController.dispose();
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
    body: Center(
      child: SlideTransition(
        position: logoAnimation,
        child: Container(
          height: MediaQuery.of(context).size.height * 0.35,
          child: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              Align(
                alignment: Alignment.lerp(Alignment.center, Alignment.topCenter, 0.8),
                child: Image(
                  image: AssetImage('assets/image/planet.png'),
                  height: 140,
                  width: 140,
                ),
              ),
              Align(
                alignment: Alignment.lerp(Alignment.bottomCenter, Alignment.bottomLeft, 0.4 ),
                child: Image(
                  image: AssetImage('assets/image/rocket.png'),
                  height: 80,
                  width: 80,
                ),
              ),
              Align(
                  alignment: Alignment.lerp(Alignment.topLeft, Alignment.center, 0.4 ),
                  child: RotationTransition(
                    turns: starsAnimation,
                    child: Image(
                      image: AssetImage('assets/image/star1.png'),
                      height: 30,
                      width: 30,
                    ),
                  )
              ),
              Align(
                  alignment: Alignment.lerp(Alignment.topLeft, Alignment.bottomCenter, 0.45 ),
                  child: Transform.rotate(
                      angle: 0.7,
                      child: RotationTransition(
                        turns: starsAnimation,
                        child: Image(
                          image: AssetImage('assets/image/star1.png'),
                          height: 20,
                          width: 20,
                        ),
                      )
                  )
              ),
              Align(
                  alignment: Alignment.lerp(Alignment.topRight, Alignment.bottomCenter, 0.5 ),
                  child: Transform.rotate(
                      angle: 0.5,
                      child: RotationTransition(
                        turns: starsAnimation,
                        child: Image(
                          image: AssetImage('assets/image/star1.png'),
                          height: 20,
                          width: 20,
                        ),
                      )
                  )
              ),
              Align(
                  alignment: Alignment.lerp(Alignment.topRight, Alignment.bottomCenter, 0.65 ),
                  child: Transform.rotate(
                      angle: -0.3,
                      child: RotationTransition(
                        turns: starsAnimation,
                        child: Image(
                          image: AssetImage('assets/image/star1.png'),
                          height: 35,
                          width: 35,
                        ),
                      )
                  )
              ),
            ],
          ),
        ),
      )
    ),
  );
}

