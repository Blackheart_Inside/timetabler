import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:timetabler/main.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/painting.dart';

class MainDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Expanded(
            child: ListView(
              padding: EdgeInsets.zero,
              children: <Widget>[
                _CustomHeader(),
                ListTile(
                  leading: Icon(Icons.person),
                  title: Text(
                    'Профиль',
                    style: TextStyle(
                      fontSize: 18,
                      fontFamily: 'Roboto Condensed',
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  onTap:() => Navigator.of(context).pushNamed(MyApp.profile),
                ),
                ListTile(
                  leading: Icon(Icons.chat),
                  title: Text(
                    'Сообщения',
                    style: TextStyle(
                      fontSize: 18,
                      fontFamily: 'Roboto Condensed',
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  onTap:() => Navigator.of(context).pushNamed(MyApp.chatRoom),
                ),
                ListTile(
                  leading: Icon(Icons.view_list),
                  title: Text(
                    'Расписание',
                    style: TextStyle(
                      fontSize: 18,
                      fontFamily: 'Roboto Condensed',
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  onTap:() =>
                      Navigator.of(context).pushNamed(MyApp.main),
                ),
                ListTile(
                  leading: Icon(Icons.person_outline),
                  title: Text(
                    'Преподаватели',
                    style: TextStyle(
                      fontSize: 18,
                      fontFamily: 'Roboto Condensed',
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  onTap:() =>
                      Navigator.of(context).pushNamed(MyApp.teachers),
                ),
                ListTile(
                  leading: Icon(Icons.info_outline),
                  title: Text(
                    'О программе',
                    style: TextStyle(
                      fontSize: 18,
                      fontFamily: 'Roboto Condensed',
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  onTap:() => Navigator.of(context).pushNamed(MyApp.aboutIt),
                ),
                ListTile(
                  leading: Icon(Icons.arrow_back),
                  title: Text(
                    'Выход',
                    style: TextStyle(
                      fontSize: 18,
                      fontFamily: 'Roboto Condensed',
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  onTap: () async {
                    final storage = FlutterSecureStorage();
                    await storage.deleteAll();
                    await FirebaseAuth.instance.signOut();
                    Navigator.of(context).pushNamedAndRemoveUntil(MyApp.auth, (_) => false);
                  },
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

class _CustomHeader extends StatelessWidget {
  Future<DocumentSnapshot> inputData() async {
    final FirebaseUser user = await FirebaseAuth.instance.currentUser();
    final CollectionReference collectionReference = Firestore.instance.collection('profiles');
    return collectionReference.document(user.uid).snapshots().first;
  }
  @override
  Widget build(BuildContext context) => Container(
      color: Colors.orange,
      child: FutureBuilder(
        future: inputData(),
        builder: (context, snapshot) => snapshot.hasData ? Center(
            child: Stack(
              children: <Widget>[
                Image(
                  width: 304,
                  height: 200,
                  image: NetworkImage(snapshot.data.data['avatar'],), fit: BoxFit.cover,
                ),
                Positioned(
                  width: 304,
                  height: 200,
                  bottom: 1,
                  child: BackdropFilter(
                    filter: ImageFilter.blur(sigmaX: 3, sigmaY: 3),
                    child: Container(
                      color: Colors.black.withOpacity(0),
                    ),
                  ),
                ),
                Container(
                  height: 70,
                  width: 70,
                  margin: EdgeInsets.only(
                    top: 45,
                    left: 20,
                  ),
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(color: Colors.grey, blurRadius: 5.0)
                      ],
                      borderRadius: BorderRadius.all(Radius.circular(100)),
                      image: DecorationImage(
                          image: NetworkImage(snapshot.data.data['avatar']), fit: BoxFit.cover)),
                ),
                Container(
                  width: 200,
                  height: 50,
                  margin: EdgeInsets.only(
                      top: 120,
                      left: 20
                    //right: 170,
                  ),
                  child: Text(snapshot.data.data['fio'],
                    style: TextStyle(fontSize: 18,
                      color: Colors.white,
                      fontFamily: 'Roboto Condensed',
                      fontWeight: FontWeight.bold,
                      shadows: [
                        Shadow( // bottomLeft
                            offset: Offset(-1.0, -1.0),
                            color: Colors.black
                        ),
                        Shadow( // bottomRight
                            offset: Offset(1.0, -1.0),
                            color: Colors.black
                        ),
                        Shadow( // topRight
                            offset: Offset(1.0, 1.0),
                            color: Colors.black
                        ),
                        Shadow( // topLeft
                            offset: Offset(-1.0, 1.0),
                            color: Colors.black
                        ),
                      ],

                    ),
                  ),
                ),
                Container(
                  width: 200,
                  height: 18,
                  margin: EdgeInsets.only(
                      top: 170,
                      left: 20
                    //right: 170,
                  ),
                  child: Text(snapshot.data.data['email'],
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'Roboto Condensed',
                      fontWeight: FontWeight.bold,
                      shadows: [
                        Shadow( // bottomLeft
                            offset: Offset(-1.0, -1.0),
                            color: Colors.black
                        ),
                        Shadow( // bottomRight
                            offset: Offset(1.0, -1.0),
                            color: Colors.black
                        ),
                        Shadow( // topRight
                            offset: Offset(1.0, 1.0),
                            color: Colors.black
                        ),
                        Shadow( // topLeft
                            offset: Offset(-1.0, 1.0),
                            color: Colors.black
                        ),
                      ],
                    ),
                  ),
                )
              ],
            )
        ) : Center(
          child: CircularProgressIndicator(),
        ),
      )
  );
}
