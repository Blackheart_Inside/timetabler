import 'package:cloud_firestore/cloud_firestore.dart';

import 'helper_func.dart';

class DatabaseMethods{

  DatabaseMethods._();
  static DatabaseMethods _instance = DatabaseMethods._();
  factory DatabaseMethods() => _instance;

  getUserByUsername(String username) async {
    return await Firestore.instance.collection("profiles")
        .where("nickname", isEqualTo: username)
        .getDocuments();
  }
  
  getUserByUserEmail(String userEmail) async {
    return await Firestore.instance.collection("profiles")
        .where("email", isEqualTo: userEmail)
        .getDocuments();
  }
  
  uploadUserInfo(userMap){
    Firestore.instance.collection("profiles")
        .add(userMap).catchError((e){
          print(e.toString());
    });
  }
  Future<bool> addChatRoom(Map<String, dynamic> chatRoom, chatRoomId) {
    Firestore.instance
        .collection("chatRoom")
        .document(chatRoomId)
        .setData(chatRoom)
        .catchError((e) {
      print(e);
    });
  }
  createChatRoom(String chatRoomId, chatRoomMap){
    Firestore.instance.collection("ChatRoom")
        .document(chatRoomId)
        .setData(chatRoomMap)
        .catchError((e){
      print(e.toString());
    });
  }
  
  addConversationMessages(String chatRoomId, messageMap){
    Firestore.instance.collection("ChatRoom")
        .document(chatRoomId)
        .collection("chats")
        .add(messageMap).catchError((e){print(e.toString());});
  }

  Stream<QuerySnapshot> getConversationMessages(String chatRoomId) {
    return Firestore.instance.collection("ChatRoom")
        .document(chatRoomId)
        .collection("chats").orderBy("time", descending: false)
        .snapshots();
  }

  Stream<QuerySnapshot> getLastMessagesForDialog(String chatRoomId) {
    return Firestore.instance.collection("ChatRoom")
        .document(chatRoomId)
        .collection("chats").orderBy("time", descending: true)
        .snapshots();
  }

  Stream<QuerySnapshot> getChatRooms(String name) => Firestore.instance
      .collection("ChatRoom")
      .where("profiles", arrayContains: name).snapshots();

  Stream<QuerySnapshot> getUserAvatar(String userName) {
    return Firestore.instance
        .collection("profiles")
        .where("nickname", isEqualTo: userName).snapshots();
  }
}