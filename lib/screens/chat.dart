import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'package:timetabler/screens/services/database.dart';
import 'package:intl/intl.dart';
import 'package:timetabler/screens/services/helper_func.dart';
class ConversationScreen extends StatefulWidget {
  final String chatRoomId;

  ConversationScreen(this.chatRoomId);

  @override
  _ConversationScreenState createState() => _ConversationScreenState();

}

class _ConversationScreenState extends State<ConversationScreen> {
  final TextEditingController messageController = TextEditingController();

  @override

  void dispose() {
    messageController.dispose();
    super.dispose();
  }
  ScrollController _scrollController = new ScrollController();
  void sendMessage() async {
    if(messageController.text.isNotEmpty){
      Map<String, dynamic> messageMap = {
        "message" : messageController.text,
        "sendBy" : await HelperFunctions.getUserNameSharedPreference(),
        "time" : FieldValue.serverTimestamp(),
      };
      DatabaseMethods().addConversationMessages(widget.chatRoomId, messageMap);
      messageController.text = "";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        title: Text('Диалог', style: TextStyle(color: Colors.white)),
      ),
      body: Container(
        child: Stack(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(bottom: 50),
                child: FutureBuilder<String>(
                  future: HelperFunctions.getUserNameSharedPreference(),
                  builder: (_, snapshotFuture) => snapshotFuture.hasData ? StreamBuilder<QuerySnapshot>(
                    stream: DatabaseMethods().getConversationMessages(widget.chatRoomId),
                    builder: (context, snapshot){
                      final List<DocumentSnapshot> _reverseList = [];
                      if(snapshot.hasData) 
                        _reverseList.addAll(snapshot.data.documents.reversed);
                      return snapshot.hasData ? ListView.builder(
                        controller: _scrollController,
                          reverse: true,
                          itemCount: _reverseList.length,
                          itemBuilder: (context, index) => MessageTile(_reverseList[index].data["message"], _reverseList[index].data["sendBy"] == snapshotFuture.data, _reverseList[index].data["time"])
                      ) : Center(
                        child: CircularProgressIndicator(),
                      );
                    },
                  ): Center(
                    child: CircularProgressIndicator(),
                  ),
                )
            ),
            Container(

              alignment: Alignment.bottomCenter,
              child: Container(
                height: 50,
                // decoration: BoxDecoration(
                //     border: Border.all(
                //         width: 1,
                //         color: Colors.orange
                //     ),
                //     borderRadius: BorderRadius.only(topLeft: Radius.circular(15),topRight: Radius.circular(15))
                // ),
                //margin: EdgeInsets.fromLTRB(15, 15, 15, 15),
                child: Row(
                  children: <Widget>[
                    Expanded(
                        child: TextField(
                          maxLines: 1,
                          controller: messageController,
                          decoration: InputDecoration(
                              labelText: "Сообщение",
                              isDense: true,
                              labelStyle: TextStyle(color: Colors.orange),
                              border: const OutlineInputBorder(
                                  borderRadius: BorderRadius.only(topLeft: Radius.circular(10)),
                                  borderSide: const BorderSide(color: Colors.orange))
                          ),
                        )
                    ),
                    GestureDetector(
                      onTap: (){
                        sendMessage();
                      },
                      child: Container(
                        margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                        height: 50,
                        width: 30,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(topRight: Radius.circular(10)),
                            color: Colors.orange,
                        ),
                        padding: EdgeInsets.all(0),
                        child: Icon(Icons.send, color: Colors.white, size: 18,),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class MessageTile extends StatelessWidget {
  final String message;
  final bool isSendByMe;
  final Timestamp time;
  MessageTile(this.message, this.isSendByMe, this.time);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onLongPress: () => showDialog(
        context: context,
        builder: (_) => new AlertDialog(
          title: Text('Удалить сообщения?'),
          actions: [
            new FlatButton(
              onPressed:() {
                Navigator.pop(context);
              },
              child: new Text('Удалить'),
            ),
            new FlatButton(
              onPressed:() {
                Navigator.pop(context);
              },
              child: new Text('Отмена'),
            ),

          ],
        ),
      ),
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.symmetric(vertical: 8),
            padding: EdgeInsets.only(left: isSendByMe ? 0 : 15, right: isSendByMe ? 15 : 0),
            width: MediaQuery.of(context).size.width,
            alignment: isSendByMe ? Alignment.centerRight : Alignment.centerLeft,
            child: Container(
              margin: isSendByMe ? EdgeInsets.fromLTRB(80, 0, 0, 0) : EdgeInsets.fromLTRB(0, 0, 80, 0),
              padding: EdgeInsets.symmetric(horizontal: 24, vertical: 16),
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 1.5,
                    blurRadius: 3,
                    offset: Offset(0, 4), // changes position of shadow
                  ),],
                color: isSendByMe ? Colors.orange : Color.fromRGBO(63, 83, 120, 1),
                borderRadius: isSendByMe ?
                BorderRadius.only(
                  topLeft: Radius.circular(23),
                  topRight: Radius.circular(23),
                  bottomLeft: Radius.circular(23),
                ) : BorderRadius.only(
                    topLeft: Radius.circular(23),
                    topRight: Radius.circular(23),
                    bottomRight: Radius.circular(23)),

              ),
              child: Text(message, style: TextStyle(
                  color: Colors.white,
                  fontSize: 17
              ),),
            ),
          ),
          Container(
            margin: isSendByMe ? EdgeInsets.only(right: 15) : EdgeInsets.only(left: 15),
            alignment: isSendByMe ? Alignment.centerRight : Alignment.centerLeft,
              child: time!=null ? Row(
                mainAxisAlignment: isSendByMe ? MainAxisAlignment.end : MainAxisAlignment.start,
                children: [
                Text("${DateFormat('kk:mm').format(time.toDate())}", style: TextStyle(fontSize: 10),),
                Icon(Icons.done, color: isSendByMe ? Colors.orange :  Color.fromRGBO(63, 83, 120, 1), size: 14,)
              ],) : SizedBox(child: CircularProgressIndicator(backgroundColor: Colors.orange, valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),strokeWidth: 1,), width: 10, height: 10)
          )
        ]
      ),
    );
  }
}
//LinearProgressIndicator(backgroundColor: Colors.orange, valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),)