import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:timetabler/screens/chat.dart';
import 'package:timetabler/screens/search.dart';
import 'package:timetabler/screens/services/helper_func.dart';
import './main_drawer.dart';
import 'package:timetabler/screens/services/database.dart';

class ChatRooms extends StatefulWidget {
  @override
  _ChatRoomsState createState() => _ChatRoomsState();
}

class _ChatRoomsState extends State<ChatRooms> {

  Widget chatRoomList(){
    return FutureBuilder<String>(
      future: HelperFunctions.getUserNameSharedPreference(),
      builder: (_, snapshotFuture) => snapshotFuture.hasData ? StreamBuilder<QuerySnapshot>(
        stream: DatabaseMethods().getChatRooms(snapshotFuture.data),
        builder: (_, snapshot) => snapshot.hasData ? ListView.builder(
            itemCount: snapshot.data.documents.length,
            itemBuilder: (context, index){
              return ChatRoomTile(snapshot.data.documents[index].data["chatRoomId"]
                  .toString().replaceAll("_", "")
                  .replaceAll(snapshotFuture.data, ""),
                  snapshot.data.documents[index].data["chatRoomId"]);
            }
        ) : Center(
          child: CircularProgressIndicator(),
        ),
      ) : Center(
        child: CircularProgressIndicator(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          title: Text("Сообщения", style: TextStyle(color: Colors.white)),
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.orange,
          child: Icon(Icons.search),
          onPressed:(){
            Navigator.push(context, MaterialPageRoute(builder: (context) => searchScreen()));
          },
        ),
        drawer: MainDrawer(),
        body: Container(
          child: chatRoomList(),
        )
    );
  }
}

class ChatRoomTile extends StatelessWidget {
  final String userName;
  final String chatRoom;
  ChatRoomTile(this.userName, this.chatRoom);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onLongPress: () => showDialog(
        context: context,
        builder: (_) => new AlertDialog(
          title: Text('Удалить диалог?'),
          actions: [
            new FlatButton(
              onPressed:() {
                Firestore.instance.collection("ChatRoom").document(chatRoom).delete();
                Navigator.pop(context);
              },
              child: new Text('Удалить'),
            ),
            new FlatButton(
              onPressed:() {
                Navigator.pop(context);
              },
              child: new Text('Отмена'),
            ),

          ],
        ),
      ),
      onTap: (){
        Navigator.push(context, MaterialPageRoute(
          builder: (context) =>ConversationScreen(chatRoom)
        ));
      },
      child: Card(
        elevation: 3,
        child: ListTile(
          title: Text(userName, style: TextStyle(color: Colors.orange, fontSize: 20),),
          subtitle: StreamBuilder<QuerySnapshot>(
              stream: DatabaseMethods().getLastMessagesForDialog(chatRoom),
              builder: (context, snapshot) => snapshot.hasData ? Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                      children: <Widget>[
                        Text("${snapshot.data.documents[0].data["sendBy"]}: ", style: TextStyle(fontSize: 12)),
                        Text(snapshot.data.documents[0].data["message"], style: TextStyle(fontSize: 12))
                      ]
                  ),
                ],
              ) : Container(child: CircularProgressIndicator(),)
          ),
          leading: Container(

            width: 58,
            height: 58,
            child: StreamBuilder<QuerySnapshot>(
              stream: DatabaseMethods().getUserAvatar(userName),
              builder: (context, snapshot) => snapshot.hasData ? ClipRRect(
                clipBehavior: Clip.antiAliasWithSaveLayer,
                borderRadius: BorderRadius.circular(57),
                child: Image(image: NetworkImage(snapshot.data.documents[0].data["avatar"]),fit: BoxFit.cover,),
              ) : Container(child: CircularProgressIndicator(),),
            ),
          ),
        ),
      ),
    );
  }

}
//NetworkImage(snapshot.data.documents[0].data["avatar"])

// StreamBuilder<QuerySnapshot>(
// stream: DatabaseMethods().getUserAvatar(userName),
// builder: (context, snapshot) => snapshot.hasData ? Image(image: NetworkImage(snapshot.data.documents[0].data["avatar"])) : Container(child: CircularProgressIndicator(),),
// )