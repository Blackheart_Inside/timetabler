import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class ProfileScreen extends StatelessWidget {
  Future<DocumentSnapshot> inputData() async {
    final FirebaseUser user = await FirebaseAuth.instance.currentUser();
    final CollectionReference collectionReference = Firestore.instance.collection('profiles');
    return collectionReference.document(user.uid).snapshots().first;
  }
   
  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: "profile",
      child: Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        title: Text('Профиль', style: TextStyle(color: Colors.white)),
      ),
      body: FutureBuilder<DocumentSnapshot>(
        future: inputData(),
        builder: (context, snapshot) => snapshot.hasData ? ListView(
          padding: EdgeInsets.all(5.0),
          children: <Widget>[
            Card(
              child: Padding(
                padding: EdgeInsets.all(20.0),
                child: Column(
                  children: <Widget>[
                    Container(
                      height: 150,
                      width: 150,
                      decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(color: Colors.grey, blurRadius: 5.0)
                          ],
                          borderRadius: BorderRadius.all(Radius.circular(100)),
                          image: DecorationImage(
                              image: NetworkImage(snapshot.data.data['avatar']), fit: BoxFit.cover)),
                    ),
                    SizedBox(height: 15),
                    Text(snapshot.data.data['fio'], style: TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 18)),
                    SizedBox(height: 5),
                    Text(
                      "Преподаватель",
                      style: TextStyle(fontSize: 16, color: Colors.orange),
                    ),
                    SizedBox(height: 25),
                    Card(
                      child: Padding(
                        padding: EdgeInsets.only(top: 20, bottom: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            Column(
                              children: <Widget>[
                                Text(
                                  snapshot.data.data['group'],
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold, fontSize: 18),
                                ),
                                Text(
                                  "Группа",
                                  style: TextStyle(color: Colors.orange),
                                )
                              ],
                            ),
                            Column(
                              children: <Widget>[
                                Text(
                                  snapshot.data.data['age'],
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold, fontSize: 18),
                                ),
                                Text(
                                  "Возраст",
                                  style: TextStyle(color: Colors.orange),
                                )
                              ],
                            ),
                            Column(
                              children: <Widget>[
                                Text(
                                  snapshot.data.data['workExp'],
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold, fontSize: 18),
                                ),
                                Text(
                                  "Стаж",
                                  style: TextStyle(color: Colors.orange),
                                )
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    Card(
                      child: Padding(
                        padding: EdgeInsets.all(20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Expanded(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "Обучает дисциплинам",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold, fontSize: 16),
                                  ),
                                  SizedBox(height: 15),
                                  Text(
                                    snapshot.data.data['lessons'],
                                    style: TextStyle(
                                      color: Colors.orange,
                                    ),
                                    softWrap: true,
                                    maxLines: 2,
                                    textAlign: TextAlign.left,
                                  ),
                                  SizedBox(height: 15),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Card(
                      child: Padding(
                        padding: EdgeInsets.all(20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Контактные данные",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold, fontSize: 16),
                                ),
                                SizedBox(height: 15),
                                Text(
                                  "Номер телефона: "+snapshot.data.data['mobile'],
                                  style: TextStyle(
                                    color: Colors.orange,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                                SizedBox(height: 15),
                                Text(
                                  "Email: "+snapshot.data.data['email'],
                                  style: TextStyle(
                                    color: Colors.orange,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                                SizedBox(height: 15),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ) : Center(
          child: CircularProgressIndicator(),
        ),
      ),
    ),
    );
  }
}

