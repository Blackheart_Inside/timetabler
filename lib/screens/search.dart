import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'package:timetabler/screens/services/database.dart';
import 'package:timetabler/screens/chat.dart';
import 'package:timetabler/screens/services/helper_func.dart';

// ignore: camel_case_types
class searchScreen extends StatefulWidget {
  @override
  _searchScreenState createState() => _searchScreenState();
}

// ignore: camel_case_types
class _searchScreenState extends State<searchScreen> {
  DatabaseMethods db = new DatabaseMethods();
  TextEditingController searchTextEditingController = new TextEditingController();
  QuerySnapshot searchSnapshot;

  initiateSearch(){
    db.getUserByUsername(searchTextEditingController.text)
        .then((e){
          setState(() {
            searchSnapshot = e;
          });
    });
  }

  createChatroomAndStartConversation({String userName}) async {
    if(userName != HelperFunctions.getUserNameSharedPreference()){
      print("${userName} ---> ${HelperFunctions.getUserNameSharedPreference()}");
      String chatRoomId = getChatRoomId(userName, await HelperFunctions.getUserNameSharedPreference());
      List<String> users = [userName, await HelperFunctions.getUserNameSharedPreference()];
      Map<String, dynamic> chatRoomMap = {
        "profiles" : users,
        "chatRoomId" : chatRoomId
      };
      db.createChatRoom(chatRoomId, chatRoomMap);
      Navigator.push(context, MaterialPageRoute(
          builder: (context) => ConversationScreen(chatRoomId)
      ));
    } else {
      print("Нельзя отправлять сообщения самому себе");
    }
  }

  Widget SearchTile({String userName, String userEmail, String userNick}){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 24,vertical: 26),
      child: Row(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(userNick, style: TextStyle(color: Colors.orange, fontSize: 16, fontWeight: FontWeight.bold)),
              Text(userName, style: TextStyle(color: Colors.orange)),
              Text(userEmail, style: TextStyle(color: Colors.orange))
            ],
          ),
          Spacer(),
          GestureDetector(
            onTap: (){
              createChatroomAndStartConversation(
                userName: userNick
              );
            },
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.orange,
                  borderRadius: BorderRadius.circular(30)
              ),
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 8),
              child: Text("Написать", style: TextStyle(color: Colors.white),),
            ),
          )
        ],
      ),
    );
  }
  Widget searchList(){
    return searchSnapshot != null ? ListView.builder(
      shrinkWrap: true,
      itemCount: searchSnapshot.documents.length,
        itemBuilder: (context, index){
        return SearchTile(
          userName: searchSnapshot.documents[index].data["fio"],
          userEmail: searchSnapshot.documents[index].data["email"],
          userNick: searchSnapshot.documents[index].data["nickname"],
        );
        },
    ) : Container();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          title: Text("Поиск пользователей", style: TextStyle(color: Colors.white)),
        ),
        body: Container(
          child: Column(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 2,
                    color: Colors.orange
                  ),
                  borderRadius: BorderRadius.circular(50)
                ),
                margin: EdgeInsets.fromLTRB(15, 15, 15, 15),
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                        child: TextField(
                          onChanged: initiateSearch(),
                          controller: searchTextEditingController,
                          decoration: InputDecoration(

                            hintText: "Имя пользователя...",
                            hintStyle: TextStyle(
                              color: Colors.orange,
                            ),
                            border: InputBorder.none

                          ),
                        )
                    ),
                    GestureDetector(
                      onTap: (){
                        initiateSearch();
                      },
                      child: Container(
                        height: 40,
                        width: 40,
                        decoration: BoxDecoration(
                          color: Colors.orange,
                          borderRadius: BorderRadius.circular(40)
                        ),
                        padding: EdgeInsets.all(9),
                        child: Icon(Icons.search, color: Colors.white,),
                      ),
                    )
                  ],
                ),
              ),
              searchList()
            ],
          ),
        )
    );
  }
  @override
  void initState() {
    super.initState();
  }


}


getChatRoomId(String a, String b){
  if(a.substring(0, 1).codeUnitAt(0) > b.substring(0, 1).codeUnitAt(0)) {
    return "$b\_$a";
  } else {
    return "$a\_$b";
  }
}