import 'package:flutter/material.dart';
import './main_drawer.dart';

class AboutProgram extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        title: Text("О программе", style: TextStyle(color: Colors.white)),
      ),
      body: Container(
        child: Center(
          child: Padding(
            padding: EdgeInsets.all(60),
            child: Text("Программа разработана Нухкади `Blackheart` Рабазанов для выпускной квалифицированной работы\n\n\n                version: 1.0.0 release", style: TextStyle(
              fontSize: 18,
              fontFamily: 'Roboto Condensed',
              fontWeight: FontWeight.bold,
              color: Colors.orange,
            ),
            ),
          ),
        ),
      )
    );
  }
}
