import 'dart:async';
import 'package:flutter/material.dart';
import 'forms/auth_form.dart';

class AuthScreen extends StatefulWidget {
  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> with TickerProviderStateMixin {
  AnimationController _controller;
  AnimationController _textAnimationController;
  AnimationController _starAnimationController;
  Animation<Offset> logoAnimation;
  Animation<double> textAnimation;
  Animation<double> starsAnimation;

  @override
  void initState() {
    //Logo animation
    _controller = AnimationController(vsync: this, duration: Duration(seconds: 1),);
    _controller.addStatusListener((status){
      if (status == AnimationStatus.completed) {
        _textAnimationController.forward();
      }
    });
    logoAnimation =  Tween<Offset>(
        begin: Offset(0, 0.5),
        end: Offset.zero
    ).animate(CurvedAnimation(parent: _controller, curve: Curves.elasticInOut));

    //Text animation
    _textAnimationController = AnimationController(vsync: this, duration: Duration(milliseconds: 400));
    textAnimation = CurvedAnimation(parent: _textAnimationController, curve: Curves.slowMiddle);

    //Star animation
    _starAnimationController = AnimationController(vsync: this, duration: Duration(seconds: 5));
    starsAnimation = CurvedAnimation(parent: _starAnimationController, curve: Curves.linear);
    _starAnimationController.repeat();
    super.initState();
    //Timer for starting animation for logo
    Timer(Duration(seconds: 2), (){
      _controller.forward();
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    _starAnimationController.dispose();
    _textAnimationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                SlideTransition(
                  position: logoAnimation,
                  child: Container(
                    height: MediaQuery.of(context).size.height * 0.35,
                    child: Stack(
                      fit: StackFit.expand,
                      children: <Widget>[
                        Align(
                          alignment: Alignment.lerp(Alignment.center, Alignment.topCenter, 0.8),
                          child: Image(
                            image: AssetImage('assets/image/planet.png'),
                            height: 140,
                            width: 140,
                          ),
                        ),
                        Align(
                          alignment: Alignment.lerp(Alignment.bottomCenter, Alignment.bottomLeft, 0.4 ),
                          child: Image(
                            image: AssetImage('assets/image/rocket.png'),
                            height: 80,
                            width: 80,
                          ),
                        ),
                        Align(
                            alignment: Alignment.lerp(Alignment.topLeft, Alignment.center, 0.4 ),
                            child: RotationTransition(
                              turns: starsAnimation,
                              child: Image(
                                image: AssetImage('assets/image/star1.png'),
                                height: 30,
                                width: 30,
                              ),
                            )
                        ),
                        Align(
                            alignment: Alignment.lerp(Alignment.topLeft, Alignment.bottomCenter, 0.45 ),
                            child: Transform.rotate(
                                angle: 0.7,
                                child: RotationTransition(
                                  turns: starsAnimation,
                                  child: Image(
                                    image: AssetImage('assets/image/star1.png'),
                                    height: 20,
                                    width: 20,
                                  ),
                                )
                            )
                        ),
                        Align(
                            alignment: Alignment.lerp(Alignment.topRight, Alignment.bottomCenter, 0.5 ),
                            child: Transform.rotate(
                                angle: 0.5,
                                child: RotationTransition(
                                  turns: starsAnimation,
                                  child: Image(
                                    image: AssetImage('assets/image/star1.png'),
                                    height: 20,
                                    width: 20,
                                  ),
                                )
                            )
                        ),
                        Align(
                            alignment: Alignment.lerp(Alignment.topRight, Alignment.bottomCenter, 0.65 ),
                            child: Transform.rotate(
                                angle: -0.3,
                                child: RotationTransition(
                                  turns: starsAnimation,
                                  child: Image(
                                    image: AssetImage('assets/image/star1.png'),
                                    height: 35,
                                    width: 35,
                                  ),
                                )
                            )
                        ),
                      ],
                    ),
                  ),
                ),
                FadeTransition(
                  opacity: textAnimation,
                  child: Text(
                    'TimeTabler',
                    style: TextStyle(
                        fontSize: 50,
                        fontFamily: "Roboto Condensed",
                        fontWeight: FontWeight.bold,
                        color: Colors.orange),
                    textAlign: TextAlign.center,
                  ),
                ),
                FadeTransition(
                  opacity: textAnimation,
                  child: AuthForm(),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
