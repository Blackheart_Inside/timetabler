import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:timetabler/main.dart';
import 'package:timetabler/screens/services/database.dart';
import 'package:timetabler/screens/services/helper_func.dart';

class AuthForm extends StatefulWidget {
  @override
  AuthFormState createState() => new AuthFormState();
}

class AuthFormState extends State<AuthForm> {
  DatabaseMethods db = new DatabaseMethods();
  final GlobalKey<FormState> _key = GlobalKey();
  final TextEditingController _controllerLogin = TextEditingController();
  final TextEditingController _controllerPassword = TextEditingController();
  QuerySnapshot snapshotUserInfo;
  String _email;
  String _password;
  // GET UID
  Stream<String> get onAuthStateChanged =>
      FirebaseAuth.instance.onAuthStateChanged.map(
            (FirebaseUser user) => user?.uid,
      );
  @override



  void dispose() {
    _controllerPassword.dispose();
    _controllerLogin.dispose();
    super.dispose();
  }
  bool validateAndSave(){
    final form = _key.currentState;
    if(form.validate()){
      form.save();
      print('Form is valid');
      return true;
    } else {
      print('Form is invalid');
      return false;
    }
  }
  void submit() async {
    print("Signed");
    if(validateAndSave()) {
      try{
        AuthResult response = await FirebaseAuth.instance.signInWithEmailAndPassword(
            email: _controllerLogin.text,
            password: _controllerPassword.text
        );
        if(response != null){
          final storage = FlutterSecureStorage();
          storage.write(key: 'login', value: _controllerLogin.text);
          storage.write(key: 'pwd', value: _controllerPassword.text);
          HelperFunctions.saveUserEmailSharedPreference(_controllerLogin.text);
          db.getUserByUserEmail(_controllerLogin.text).then((val){
            snapshotUserInfo = val;
            HelperFunctions.saveUserNameSharedPreference(snapshotUserInfo.documents[0].data["nickname"]);
            //print("${snapshotUserInfo.documents[0].data["nickname"]}    fuuuuuuuuuuuuuuuuuuck");
          });
        }
        HelperFunctions.saveUserLoggedInSharedPreference(true);
        Navigator.of(context).pushNamedAndRemoveUntil(MyApp.main, (route) => false);
        print('Login: ${_controllerLogin.text}\n Password: ${_controllerPassword.text}');
      }catch(e){
        print('Error: $e');
        Scaffold.of(context).showSnackBar(const SnackBar(content: Text('Неверный email или пароль'), backgroundColor: Colors.redAccent,));
      }
    }
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 25, horizontal: 25 ),
      child: Form(
        key: _key,
        child: Column(
          children: <Widget>[
            TextFormField(
              style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Roboto Condensed'
              ),
              controller: _controllerLogin,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.vertical(
                      top: Radius.circular(35),
                    ),
                    borderSide: BorderSide(
                      color: Colors.orange,
                    )
                ),
                hintText: 'Введите свой email',
              ),
              validator: (value) => value.isEmpty ? 'Empty email' : null,
              onSaved: (value) => _email = value,
            ),
            TextFormField(
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
                fontFamily: 'Roboto Condensed'
              ),
              obscureText: true,
              controller: _controllerPassword,
              decoration: InputDecoration(
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.vertical(
                      bottom: Radius.circular(35),
                    ),
                    borderSide: BorderSide(
                      color: Colors.orange,
                    )
                ),
                hintText: 'Введите свой пароль',

              ),
              validator: (value) => value.isEmpty ? 'Empty password' : null,
              onSaved: (value) => _password = value,
            ),
            RaisedButton(
                color: Colors.orange,
                onPressed: (){
                  submit();
                }, child: Text('Войти',
              style: TextStyle(
                fontSize: 20,
                fontFamily: "Roboto Condensed",
                fontWeight: FontWeight.bold,
                color: Colors.white,
              ),))
          ],
        ),
      ),
    );
  }
}
