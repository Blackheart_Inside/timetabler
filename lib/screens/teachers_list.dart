import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:timetabler/screens/main_drawer.dart';
import 'package:timetabler/main.dart';

class TeachersList extends StatelessWidget {
  final CollectionReference dbProfiles = Firestore.instance.collection('profiles');
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          title: Text(
            "Список преподавателей",
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'Roboto Condensed',
              fontSize: 22,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        drawer: MainDrawer(),
        body: StreamBuilder<QuerySnapshot>(
            stream: Firestore.instance
                .collection("profiles")
                .where('status', isEqualTo: "Преподаватель")
                .snapshots(),
            builder: (context, snapshot) => snapshot.hasData ? snapshot.data.documents.isEmpty ? Center(
              child: Text('Нет элементов'),
            ) : FutureBuilder<FirebaseUser>(
              future: FirebaseAuth.instance.currentUser(),
              builder: (context, snapshotFuture) {
                final List<DocumentSnapshot> listTeacher = <DocumentSnapshot>[];
                if(snapshotFuture.hasData)
                  listTeacher.addAll(snapshot.data.documents.where((e) => e.documentID != snapshotFuture.data.uid).toList());
                return snapshotFuture.hasData ? ListView.builder(
                  itemCount: listTeacher.length,
                  itemBuilder: (context, index) => _TeacherListCard(listTeacher[index]),
                ) : const Center(
                  child: CircularProgressIndicator(),
                );
              },
            ) : const Center(
              child: CircularProgressIndicator(),
            )
        ),
      ),
    );
  }
}

class _TeacherListCard extends StatelessWidget {
  _TeacherListCard(this.entity);
  final DocumentSnapshot entity;
  @override
  Widget build(BuildContext context) => ListTile(
    leading: ExcludeSemantics(
      child: CircleAvatar(
        backgroundColor: Colors.orange,
        backgroundImage: NetworkImage(entity["avatar"]),
      ),
    ),
      title: Text(entity["fio"], style: TextStyle(color: Colors.orange),),
    onTap:() => Navigator.of(context).pushNamed(MyApp.otherProf, arguments: entity["fio"]),
  );
}

