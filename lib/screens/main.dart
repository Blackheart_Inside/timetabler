import 'package:flutter/material.dart';
import 'package:timetabler/main.dart';
import 'package:timetabler/screens/main_drawer.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) => StreamBuilder<QuerySnapshot>(
      stream: Firestore.instance.collection("Groups").snapshots(),
      builder: (context, groupSnapshot) => groupSnapshot.hasData ? DefaultTabController(
          length: groupSnapshot.data.documents.length,
          child: Theme(
            data: Theme.of(context).copyWith(
              tabBarTheme: TabBarTheme(
                labelColor: Colors.white
              )
            ),
            child: Scaffold(
              appBar: AppBar(
                iconTheme: IconThemeData(color: Colors.white),
                bottom: TabBar(
                  isScrollable: true,
                  tabs: groupSnapshot.data.documents.map<Widget>((e) => Tab(text: e.documentID,)).toList(),
                ),
                title: Text(
                  'Главная',
                  style: TextStyle(
                    color: Colors.white,
                    fontFamily: 'Roboto Condensed',
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              drawer: MainDrawer(),
              body: TabBarView(
                children: groupSnapshot.data.documents.map<Widget>((e) => StreamBuilder<QuerySnapshot>(
                    stream: Firestore.instance.collection("TimeTables").where('group', isEqualTo: e.documentID).snapshots(),
                    builder: (context, snapshot) => snapshot.hasData ? snapshot.data.documents.isEmpty ? Center(
                      child: Text('Нет элементов'),
                    ) : ListView.builder(
                      itemCount: snapshot.data.documents.length,
                      itemBuilder: (context, index) => _TimeTablesTile(snapshot.data.documents[index]),
                    ) : const Center(
                      child: CircularProgressIndicator(),
                    )
                )).toList(),
              ),
            ),
          ))  : const Scaffold(body:  Center(child: CircularProgressIndicator()),
      )
  );
}

class _TimeTablesTile extends StatelessWidget {
  _TimeTablesTile(this.entity);
  final DocumentSnapshot entity;
  @override
  Widget build(BuildContext context) => Card(
    child: ListTile(
      leading: ExcludeSemantics(
        child: CircleAvatar(
            backgroundColor: Colors.orange,
            child: Text(entity["time"], style: TextStyle(
              fontFamily: 'Roboto Condensed',
              color: Colors.white,
              fontSize: 14,
              fontWeight: FontWeight.bold,
            ),)),
      ),
      title: Text(entity["lesson"]),
      subtitle: Text(
          "Преподаватель: " + entity["teacher"] + "\nАудитория: " + entity["cab"]
      ),
      isThreeLine: true,
    ),
  );
}
